import React from 'react';
import {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchWeather, getWeatherByCoordinates } from '../redux/action';
// import { useHistory } from 'react-router-dom';
import { Container,Image, Header } from 'semantic-ui-react';


const HomePage = () => {
    const [lon, setLon] = useState(0);
    const [lat, setLat] = useState(0);

    const [city, setCity] = useState('London');

    const weather = useSelector((state) => state);
    const dispatch = useDispatch();

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((position) => {
            console.log("latitude is: ", position.coords.latitude);
            console.log("longitude is: ", position.coords.longitude);
            setLat(position.coords.latitude);
            setLon(position.coords.longitude);
        })
        dispatch(getWeatherByCoordinates(lat,lon));
    }, [lat,lon, dispatch]);

    useEffect(() => {
        dispatch(fetchWeather(city));
    }, [city, dispatch]);

    const handleChange = (e) => {
        (setCity(e.target.value));
    }

    // let history = useHistory();
    // const redirectClick = () => {
    //     history.push("/contacts")
    // }

    if (!weather.weather){
        return <p>Loading ...</p>;
    } else {
    return (
        <Container text >
            < Image 
            src={`http://openweathermap.org/img/wn/${weather.weather.weather[0].icon}@2x.png`}
            size='small' />
            <Header as='h1'>Country: {weather.weather.sys.country}</Header>
            <Header as='h1'>City: {weather.weather.name}</Header>
            <Header as='h2'>Latitude: {weather.weather.coord.lat} </Header>
            <Header as='h2'>Longitude: {weather.weather.coord.lon} </Header>
            <Header as='h2'>Description: {weather.weather.weather[0].main} </Header>
            <Header as='h2'>Wind: {weather.weather.wind.speed} m/s</Header>
            <Header as='h2'>
                    Temperature: {Math.ceil(weather.weather.main.temp - 273)}&#8451; 
            </Header>
            <select onChange={handleChange}>
                <option value='Kyiv'>Kyiv</option>
                <option value='Ternopil'>Ternopil</option>
                <option value='Sofia'>Sofia</option>
                <option value='Cherkasy'>Cherkasy</option>
            </select>
            {/* <div className="button__ui" onClick={redirectClick}>Contacts</div> */}
        </ Container>
    );}
}

export default HomePage;