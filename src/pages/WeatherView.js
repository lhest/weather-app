import React from 'react';
import {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchWeather } from '../redux/action';
import { useHistory } from 'react-router-dom';
import { Container,Image, Header, Button } from 'semantic-ui-react';


const WeatherView = (city) => {

    const weather = useSelector((state) => state);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchWeather(city));
    }, [city, dispatch]);
    
    let history = useHistory();
    const handleClick = () => {
        history.pushState("/");
    };
    const redirectClick = () => {
        history.pushState("/contacts")
    }
    console.log(weather);
    return (
        <Container text >
            < Image 
            src={`http://openweathermap.org/img/wn/${weather.weather.weather[0].icon}@2x.png`}
            size='small' />
            <Header as='h1'>Country: {weather.weather.sys.country}</Header>
            <Header as='h1'>City: {weather.weather.name}</Header>
            <Header as='h2'>Latitude: {weather.weather.coord.lat} </Header>
            <Header as='h2'>Longitude: {weather.weather.coord.lon} </Header>
            <Header as='h2'>Description: {weather.weather.weather[0].main} </Header>
            <Header as='h2'>Wind: {weather.weather.wind.speed} m/s</Header>
            <Header as='h2'>
                    Temperature: {Math.ceil(weather.weather.main.temp - 273)}&#8451; 
            </Header>
            <Button onClick={handleClick}>Back</Button>
            <Button onClick={redirectClick}>Contacts</Button>
        </Container>
    )
}

export default WeatherView;