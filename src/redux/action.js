import { FETCH_WEATHER, GET_CITY } from "./types";

export const getCity = city => ({
    type: GET_CITY,
    payload: city
})

export const getWeather = data => ({
  type: FETCH_WEATHER,
  payload: data
});

export const fetchWeather = city => async dispatch => {
  try {
    const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=bd71345693d3c57b7742e27f41d36a4d`)
    const data = await response.json();
    dispatch(getWeather(data));
  } catch (error) {
    console.log(error);
  }
};

export const getWeatherByCoordinates = (latitude, longitude) => async dispatch => {
    try {
        const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=bd71345693d3c57b7742e27f41d36a4d`);
        const data = await response.json();
        dispatch(getWeather(data));
    } catch (error) {
        console.log(error);
    }
};
