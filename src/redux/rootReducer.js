import { FETCH_WEATHER, GET_CITY} from './types';

const initialState = {}

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_WEATHER:
        return {
            ...state,
            weather: action.payload
        }
        case GET_CITY:
            return {
                ...state,
                city: action.payload
            }
      default:
        return state
    }
  }

