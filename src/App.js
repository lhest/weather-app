import React from 'react';
import { Router, Switch, Route} from 'react-router-dom';
import HomePage from './pages/HomePage';
import WeatherView from './pages/WeatherView';
import Contacts from './pages/Contacts';
import 'semantic-ui-css/semantic.min.css';
import './App.css'

const App = () => {
        return (
            <HomePage />
            // <Router>
            //     <Switch>
            //         <Route exact path="/" component={HomePage} />
            //         <Route path="/about" component={WeatherView} />
            //         <Route path="/contacts" component={Contacts} />
            //     </Switch>
            // </Router>
    );
}

export default App;
